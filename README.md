# Java 4 Kids
## Kinder lernen Programmieren mit Java

Die Informatik braucht dringend Nachwuchs. Je früher Kinder die Paradigmen und
Terminologie der Programmierung kennen lernen, um so einfach haben sie es später
in Lehre und Studium.

Daher unterstützt die **Java User Group Goldstadt** (JUG PF) die regionale Jugend
durch unterschiedliche Angebote unter dem Titel *Java 4 Kids*.

Eine erste Veranstaltung soll bald stattfinden für Kinder ab 12 Jahren
mit dem Ziel, die Programmiersprache Java zu erlernen. Hierzu hat die JUG PF
einen einfachen Flipperautomaten aus Holz gebaut, welchem mit einem **Raspberry
Pi** und dem eigens entwickelten Framework *JFlipper* Leben eingehaucht
wird. (@gzilly)

**:pushpin: *Jetzt Ticket buchen!* Die JUG PF präsentiert den Java-Flipper sowie das
dazu entwickelte Framework *JFlipper* auf der Konferenz "JavaLand 2017"!
![Java-Land Banner](http://www.javaland.eu/fileadmin/images/2017/2016_08_15-JavaLand_2017-Banner_Speaker-180x180.jpg)**
